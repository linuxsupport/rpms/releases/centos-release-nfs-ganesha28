Summary: NFS-Ganesha 2.8 packages from the CentOS Storage SIG repository
Name: centos-release-nfs-ganesha28
Version: 1.0
Release: 3%{?dist}
License: GPLv2
URL: http://wiki.centos.org/SpecialInterestGroup/Storage
Source0: CentOS-NFS-Ganesha-28.repo
BuildArch: noarch
Epoch: 666

%if 0%{?centos} >= 8
# $contentdir for altarch support was added with CentOS-7.5
Requires: centos-release >= 7-5.1804.el8.centos.2
%endif
# This provides the public key to verify the RPMs
Requires: centos-release-storage-common

Provides: centos-release-nfs-ganesha = 28

%description
yum configuration for NFS-Ganesha 2.8 packages from the CentOS Storage SIG.
NFS-Ganesha 2.8 will receive updates for approximately 12 months. For more
details about the release and maintenance schedule, see
https://github.com/nfs-ganesha/nfs-ganesha/wiki

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/CentOS-NFS-Ganesha-28.repo
%if 0%{?centos} < 8
sed -i 's/i\$contentdir/centos/g' %{buildroot}%{_sysconfdir}/yum.repos.d/CentOS-NFS-Ganesha-28.repo
%endif

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-NFS-Ganesha-28.repo

%changelog
* Fri Mar 20 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-3
- build for CERN

* Wed Jun 19 2019 Giulio Fidente <gfidente@redhat.com> - 1.0-2
- Fix stale references to Gluster in the .repo file

* Tue Jun 11 2019 Kaleb S KEITHLEY <kkeithle at redhat.com> - 1.0-1
- NFS-Ganesha 2.8
